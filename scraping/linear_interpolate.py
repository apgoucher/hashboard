
with open('coinprice.csv') as f:
    lines2 = [l.strip().split(',') for (i, l) in enumerate(f) if (i > 0)]
lines2 = [[int(x[0]), float(x[1])] for x in lines2]

lines2.append([2**32, lines2[-1][-1]])

with open('phases3.csv') as f:
    lines = [l.strip() for l in f]

i = 0

for j in range(1, len(lines)):
    x = lines[j].split(',')
    ts = int(x[2])
    while lines2[i+1][0] < ts:
        i += 1
    if (lines2[i][0] > ts):
        continue
    p = ((lines2[i+1][0] - ts) * lines2[i][1] + (ts - lines2[i][0]) * lines2[i+1][1]) / (lines2[i+1][0] - lines2[i][0])
    if (j == len(lines)-1):
        p = lines2[-1][-1]
    x[5] = str(int(1.0e-8 * float(x[4]) * p))
    lines[j] = ','.join(x)

with open('phases4.csv', 'w') as f:
    for l in lines:
        f.write(l + '\n')
