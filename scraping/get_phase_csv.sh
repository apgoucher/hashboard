
# Download last week of data from LoyceV's website:
echo "id,hash,time,median_time,size,stripped_size,weight,version,version_hex,version_bits,merkle_root,nonce,bits,difficulty,chainwork,coinbase_data_hex,transaction_count,witness_count,input_count,output_count,input_total,input_total_usd,output_total,output_total_usd,fee_total,fee_total_usd,fee_per_kb,fee_per_kb_usd,fee_per_kwu,fee_per_kwu_usd,cdd_total,generation,generation_usd,reward,reward_usd,guessed_miner" > blockdata.csv
curl "https://loyce.club/bitmover/blockdata.lastweek.txt" >> blockdata.csv

set -e

curl "https://catagolue.hatsya.com/textsamples/xs0_phases/b3s23/synthesis" > phases.csv
curl "https://catagolue.hatsya.com/textsamples/xs0_onchain/b3s23/synthesis" > onchain.csv
curl "https://catagolue.hatsya.com/textsamples/xs0_newblocks/b3s23/synthesis" > newblocks.csv
curl "https://catagolue.hatsya.com/textsamples/xs0_coinprice/b3s23/synthesis" > coinprice.csv

curl 'https://api.blockchair.com/bitcoin/blocks?limit=90' > output.json

python3 blocks_to_phases.py
python3 linear_interpolate.py
python3 upload_csv.py "$1" phases4.csv xs0_phases onchain3.csv xs0_onchain newblocks3.csv xs0_newblocks coinprice.csv xs0_coinprice

