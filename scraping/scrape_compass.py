#!/usr/bin/env python

from selenium import webdriver
from time import sleep
from bs4 import BeautifulSoup
import numpy as np
import pandas as pd
import os
import sys
import unicodedata
import json
from matplotlib import pyplot as plt

this_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.join(this_dir, '..')

sys.path.append(parent_dir)

from hashboard import *
from hashboard.history import download_csv
from datetime import datetime, timedelta


def get_url(driver, url):
    '''
    Retrieve the HTML of a dynamic webpage. This uses a headless Chromium
    web browser to load the page and execute the resident JavaScript code
    required to render the page. It waits 5 seconds to ensure the page is
    fully loaded.
    '''

    driver.get(url)
    sleep(5.0)
    return driver.execute_script("return document.body.innerHTML;")


def get_asic_models(driver):
    '''
    This scrapes all ASIC models from the Compass Mining homepage.
    '''

    driver.get("https://compassmining.io/hardware")

    asics = []

    for i in range(20):

        to_break = (len(asics) > 0)

        sleep(10.0)
        html_source = driver.execute_script("return document.body.innerHTML;")
        asics = [x.split('"')[0] for x in html_source.split('hardware/')[1:]]
        asics = set([x.replace('&amp;', '&') for x in asics])

        if to_break:
            break

        print("Webpage not loaded; continuing to wait...")

    return list(asics)

def parse_float(x):

    return float(x.replace(',', '').replace(' ', '').strip())

def parse_power(x):

    return parse_float(x.lower().replace('watts', '').replace('watt', '').replace('w', '').replace('k', 'e3'))

def parse_price(x):

    return parse_float(x.replace('$', ''))

def parse_hashrate(x):
    '''
    Assume TH/s unless otherwise specified.
    The units {MH/s, GH/s, TH/s, PH/s, EH/s} are supported.
    '''

    x = x.strip().replace(' ', '').replace('/', '').replace('E', 'e6').lower().replace('s', '').replace('h', '')
    return parse_float(x.replace('t', '').replace('m', 'e-6').replace('g', 'e-3').replace('p', 'e3'))

def parse_quantity(x):

    try:
        return int(x.replace(',', '').replace(' ', '').replace('+', '').strip())
    except ValueError:
        return 1

def parse_bs_text(t, url):

    t = t.replace('$\n', '$').replace('\nDetails\n', '\n').replace('Second Hand', 'Used')

    lines = [x.strip() for x in t.split('\n')]

    algorithm = 'unknown'
    hashrate = 'unknown'
    power = 'unknown'
    vendor = 'unknown'
    model = 'unknown'

    possibilities = []
    mo = 0

    for (i, l) in enumerate(lines[:-1]):

        if l.startswith('> ') and (model == 'unknown'):
            model = l[2:]

        if l.startswith('The ') and (' is a ' in l) and (vendor == 'unknown'):
            x = l[4:].split(' is a ')[0].split(' by ')
            if len(x) == 2:
                model, vendor = tuple(x)

        if l.startswith('Algorithm') and (algorithm == 'unknown'):
            algorithm = lines[i+1]
        elif l.startswith('Hashrate') and (hashrate == 'unknown'):
            hashrate = lines[i+1]
        elif l.startswith('Power') and (power == 'unknown'):
            power = lines[i+1]
        elif l == 'All' and (model == 'unknown'):
            model = lines[i+1]

    items = ['Facility\n' + x.split('\nBuy now')[0] for x in '\n'.join(lines).split('\nFacility\n')[1:]]
    items = [x.split('\n') for x in items]
    items = [(list(x[0::2]), list(x[1::2])) for x in items]
    items = [(k, v) for (k, v) in items if (len(k) == len(v)) and ('Price' in k)]

    if vendor == 'unknown':
        # guess vendor from model name if unknown
        if 'antminer' in model.lower():
            vendor = 'Bitmain'
        elif 'whatsminer' in model.lower():
            vendor = 'MicroBT'
        elif 'avalon' in model.lower():
            vendor = 'Canaan'

    if len(items) >= 1:

        for keys, values in items:
            possibilities.append({'Machine': url, 'Vendor': vendor, 'Model': model, 'MOQ': '1', 'Algorithm': algorithm, 'Hashrate': hashrate, 'Power': power})
            for (k, v) in zip(keys, values):
                possibilities[-1][k] = v

    else:

        print('Warning: no options found for %s' % url)

    things = {}

    for x in possibilities:
        if ('Price' in x) and ('$' in x['Price']) and ('Facility' in x):
            qty = parse_quantity(x.get('Available Quantity', '1'))
            frozen = tuple([(k.strip(), v.strip()) for (k, v) in x.items() if (k != 'Available Quantity')])
            if frozen not in things:
                things[frozen] = qty
            else:
                things[frozen] += qty

    possibilities = [{a : b for (a, b) in (k + (('Available Quantity', qty),))} for k, qty in things.items()]

    return possibilities


def compute_dates_and_fees(possibilities):

    for p in possibilities:

        if ('Estimated Online Date' not in p) and ('Estimated Shipping Date' in p):
            # Assume that it takes a fortnight between shipping and deployment:
            try:
                p['Estimated Online Date'] = (parse_date(p['Estimated Shipping Date']) + timedelta(14)).strftime('%Y-%m-%d')
            except:
                print('\033[31;1mError: could not parse date %s \033[0m' % p['Estimated Shipping Date'])

        if '.' not in p.get('Hosting Rate', ''):
            hosting_fee = p.get('Hosting Fee', p.get('Hosting fee', ''))
            if '.' in hosting_fee:
                # Calculate from power and hosting fee:
                p['Hosting Rate'] = '%.4f' % (parse_price(hosting_fee) / (parse_power(p['Power']) * 0.7296))
            else:
                # Typical fee for hosting providers:
                p['Hosting Rate'] = '0.09'


def get_options_for_asic(driver, asic):
    '''
    This scrapes all hosting options (combinations of location, price,
    date, etc.) for a particular model of ASIC.
    '''

    url = "https://compassmining.io/hardware/" + asic
    html_source = get_url(driver, url)
    soup = BeautifulSoup(html_source)
    t = soup.get_text('\n')

    return parse_bs_text(t, url)


def get_driver(chrome_path='/usr/bin/chromedriver'):

    chrome_options = webdriver.chrome.options.Options()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--headless")
    chrome_service = webdriver.chrome.service.Service(chrome_path)
    driver = webdriver.Chrome(service=chrome_service, options=chrome_options)
    return driver


def parse_median(s):

    return np.median([parse_hashrate(x.strip().split()[-1]) for x in s.strip().replace('/s', '').replace('-', '/').split('/')])


def parse_luxor(a):

    d = {'Algorithm': 'SHA256', 'Facility': 'Home Delivery', 'Machine': 'https://brokerage.luxor.tech/dashboard'}
    d['Hashrate'] = int(np.round(parse_median(a['hashrate'])))
    reciprocal_efficiency = float(parse_median(a['efficiency']))

    d['Power'] = '%d Watts' % int(d['Hashrate'] * reciprocal_efficiency)
    d['Price'] = '$%d' % int(d['Hashrate'] * parse_price(a['price']))
    d['Hashrate'] = '%d TH/s' % d['Hashrate']

    essd = a.get('estimatedShippingStartTime', '').strip()
    essd = (essd or '7 days').replace('business ', '')

    d['Model'] = a.get('model', 'Unknown')
    d['Vendor'] = a.get('manufacturer', 'Unknown')
    d['Estimated Shipping Date'] = essd
    d['Available Quantity'] = parse_quantity(a.get('available', '1'))
    d['Condition'] = a.get('hardwareType')
    d['MOQ'] = a.get('moq', '1')

    return d


def get_luxor_asics(driver):

    x = get_url(driver, "https://luxor.tech/brokerage/dashboard")
    x = x[x.index('{"props"'):]; x = x[:x.index('</')]
    x = json.loads(x)['props']['pageProps']['hardwareData']
    asics = [y for y in x if (y.get('hardwareType', '') in ['New', 'Used']) and ('$' in y.get('price', '')) and (y.get('efficiency', '').replace('/', '').strip())]

    return [parse_luxor(a) for a in asics]


def get_all_asics(chrome_path='/usr/bin/chromedriver'):
    '''
    Scrape all ASICs from the Compass Mining hardware page.
    '''

    driver = get_driver(chrome_path=chrome_path)

    asics = get_asic_models(driver)
    print("%d ASIC models detected: %s" % (len(asics), asics))

    if len(asics) == 0:
        exit(2)

    output = []
    # output += get_luxor_asics(driver)
    for p in output:
        print(str(p))

    for asic in asics:
        possibilities = get_options_for_asic(driver, asic)
        for p in possibilities:
            print(str(p))
        output += possibilities

    compute_dates_and_fees(output)

    driver.quit()

    return output


def remove_accents(input_str):

    nfkd_form = unicodedata.normalize('NFKD', input_str)
    only_ascii = nfkd_form.encode('ASCII', 'ignore').decode()
    return only_ascii


def asics_to_dataframe(asics, hashprice=None):
    '''
    Converts a list of ASICs (represented as dicts) into a pandas DataFrame
    containing only the hosted Bitcoin miners. This function attempts to
    normalise dates and units to result in an output suitable for automated
    postprocessing.
    '''

    # Filter ASICs to only include hosted Bitcoin miners:
    asics2 = [a for a in asics if ('Estimated Online Date' in a) and (a.get('Algorithm', '') == 'SHA256') and ('0' in a.get('Hosting Rate', ''))]

    # Cast to pandas dataframe:
    df = pd.DataFrame(asics2)

    # Parse human-readable dates/units into machine-readable ones:
    # df['MOQ'] = df['MOQ'].apply(parse_quantity)
    df['Estimated Online Date'] = df['Estimated Online Date'].apply(parse_date)
    df['Price'] = df['Price'].apply(lambda x : int(np.round(parse_price(x))))
    df['Power'] = df['Power'].apply(lambda x : int(np.round(parse_power(x))))
    df['Terahashes'] = df['Hashrate'].apply(parse_hashrate)
    df['Hosting Rate'] = df['Hosting Rate'].apply(lambda x : float(x.replace('$', ' ').replace('/', ' ').strip().split()[0]))

    # Facilities tend to contain commas; remove these for the benefit of dumb CSV readers.
    # We also strip any diacritical marks, such as the accented 'e' in 'Quebec', because
    # it seems to induce mojibake on certain platforms.
    df['Facility'] = df['Facility'].apply(lambda x : remove_accents(x.replace(',', '')))

    # Compute auxiliary values. The factor of 0.024 is:
    # (hours per day) / (watts per kilowatt)
    # because the hosting rate is quoted in $/kWh whereas power is in watts.
    df['Daily Cost'] = 0.024 * df['Power'] * df['Hosting Rate']

    if hashprice is not None:
        df['Daily Profit'] = df['Terahashes'] * hashprice - df['Daily Cost']

        # Format daily dollar amounts to 2 decimal places:
        df['Daily Profit'] = df['Daily Profit'].apply(lambda x : float('%.2f' % x))
    df['Daily Cost'] = df['Daily Cost'].apply(lambda x : float('%.2f' % x))

    # Compute efficiency and round to 2 significant figures.
    df['Efficiency'] = (df['Terahashes'] * 1.0e6 / df['Power']).apply(lambda x : int(float('%.2g' % x)))

    # An ASIC is worth running provided that the current hashprice
    # (see https://data.hashrateindex.com/network-data/btc for charts and
    # https://compassmining.io/education/what-is-hashprice/ for explanation)
    # is greater than the breakeven hashprice computed below:
    df['Breakeven Hashprice'] = df['Daily Cost'] / df['Terahashes']

    # Compute cost per terahash and sort primarily on that because it tends
    # to be the dominant factor:
    df['Price Per Terahash'] = df['Price'] / df['Terahashes']

    # Remove ASICs with zero daily cost; these are spurious entries:
    df = df[df['Daily Cost'] > 1.0e-6]

    return df.sort_values(by=['Price Per Terahash', 'Breakeven Hashprice']).copy()


def augment(df, tl):

    gg_btc = []
    gg_usd = []

    for _, p in df.iterrows():

        x = tl.evaluate_miner(p['Price'], p['Terahashes'], p['Daily Cost'], p['Estimated Online Date'])

        goodness_btc = int(np.round(100.0 * (x['daily_btc_net'].sum(axis=0) >= x['miner_btc_cost']).mean()))
        goodness_usd = int(np.round(100.0 * (x['daily_usd_net'].sum(axis=0) >= x['miner_usd_cost']).mean()))

        gg_btc.append(goodness_btc)
        gg_usd.append(goodness_usd)
    
    df['ProbUSD'] = gg_usd
    df['ProbBTC'] = gg_btc


def get_timeline(difficulty = None):

    sim = Simulator()

    # make a beautiful plot of price, hashrate, and difficulty, and save
    # it in SVG format so that it can be uploaded to Catagolue:
    ax = sim.plot_history()
    ax.patch.set_facecolor('white')
    ax.patch.set_alpha(0.5)
    plt.title('Price, hashrate, and difficulty over last four years', fontsize=18)
    plt.savefig('btcchart.svg', facecolor='None')

    avg_reward = float(np.exp(sim.history['log_reward'].iloc[-21:]).mean())
    btc_price = float(sim.history['price'].iloc[-1])

    if difficulty is None:
        difficulty = 2.0 ** (float(sim.history['zero_bits'].iloc[-1]) - 32)

    hashprice = 8.64e16 * avg_reward * btc_price / (difficulty * (2**32))

    print('Simulating 1250 random rollouts for 360000 blocks...')
    rollouts = sim.run_rollouts(1250, n_iters=7500)

    print('Converting block time to wall-clock time...')
    tl = rollouts.to_timeline()

    return tl, btc_price, rollouts.current_hashrate, avg_reward, hashprice, difficulty


def main(argv):

    if len(argv) < 3:
        print("Usage: ./scrape_compass.py /path/to/chromedriver output_asics.csv")
        exit(1)

    newblocks = download_csv('newblocks')
    height = int(newblocks['id'].iloc[-1])
    datestring = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
    difficulty = float(newblocks['difficulty'].iloc[-1])

    tl, current_price, current_hashrate, avg_reward, hashprice, difficulty = get_timeline(difficulty)

    # compute next retarget time and difficulty:
    remaining_blocks = (2015 - (height % 2016))
    last_retarget_height = height - (height % 2016)
    last_ts = pd.to_datetime(newblocks[newblocks['id'] == last_retarget_height].iloc[0]['time'])
    elapsed_seconds = (pd.to_datetime(datestring) - last_ts).total_seconds()
    remaining_seconds = remaining_blocks * (difficulty * (2**32)) / current_hashrate
    next_difficulty = difficulty * max(0.25, min(4.00, 1209600.0 / (elapsed_seconds + remaining_seconds)))

    print('Downloading ASICs from Compass...')
    asics = get_all_asics(argv[1])

    print('Computing statistics...')
    df = asics_to_dataframe(asics, hashprice)

    augment(df, tl)
    df['WeightedScore'] = 0.75 * df['ProbBTC'] + 0.25 * df['ProbUSD']
    df = df.sort_values(by='WeightedScore', ascending=False)

    # Rearrange columns:
    newcols    =   ["Machine", "Model", "Vendor", "Price", "Terahashes", "Power", "Efficiency", "Hosting Rate",
                    "Daily Cost", "Daily Profit", "Price Per Terahash", "Breakeven Hashprice", "Estimated Online Date",
                    "ProbBTC", "ProbUSD", "Facility", "Condition", "MOQ"]

    df = df[newcols].copy()

    # Rename columns:
    newcols[3:15] = ["Price ($)", "Hashrate (TH/s)", "Power (W)", "Efficiency (MH/J)",
                    "Hosting Rate ($/kWh)", "Daily Cost ($/day)", "Current Profit ($/day)", "Terahash Price ($/(TH/s))",
                    "Breakeven Hashprice ($/(TH/s)/day)", "Estimated Online Date", "ProbBTC (%)", "ProbUSD (%)"]
    df.columns = newcols

    # Export CSV:
    with open(argv[2], 'w') as f:
        f.write('# bitcoin price = <b><font color="#006400">%.2f USD</font></b>\n' % current_price)
        f.write('# global hashrate (estimated) = %.2f EH/s\n' % (1.0e-18 * current_hashrate))
        f.write('# difficulty = <b><font color="blue">%.2f T</font></b>\n' % (1.0e-12 * difficulty))
        f.write('# next difficulty (estimated) = %.2f T (<b><font color="%s">%+.2f%s</font></b>)\n' % (1.0e-12 * next_difficulty,
            ('green' if (next_difficulty > difficulty) else 'red'), 100.0 * (next_difficulty - difficulty) / difficulty, '%'))
        f.write('# block reward (7-day average) = %.3f BTC\n' % avg_reward)
        f.write('# hashprice index = <b><font color="#b8860b">%.3g $/(TH/s)/day</font></b>\n' % hashprice)
        f.write('# block height = %d\n' % height)
        f.write('# current page last updated on %s UTC\n' % datestring)
        df.to_csv(f, index=False, float_format='%.3g')

if __name__ == '__main__':

    main(sys.argv)

