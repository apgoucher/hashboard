
'''
Please do not attempt to run this file yourself. This runs as part of a
scheduled task in order to keep the following URL up to date:

https://catagolue.hatsya.com/textsamples/xs0_asics/b3s23/synthesis/asics.csv

This will not work if you try to run it, since it depends on a 160-bit
secret key being provided.
'''

from sys import argv
from urllib.request import urlopen, Request

if __name__ == '__main__':

    address = "https://catagolue.hatsya.com"
    payload = "%s SYNTH b3s23\n" % argv[1]

    for i in range(2, len(argv), 2):

        payload += "\n#CSYNTH %s costs 0 gliders.\n" % argv[i+1]

        with open(argv[i]) as f:
            payload += f.read()

        payload += "This line contains an exclamation mark!\n\n"

    req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
    f = urlopen(req)

    print(f.read())
