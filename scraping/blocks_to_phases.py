import time

unixtime = int(time.time())
usd_per_btc = 0

import json

with open('output.json') as f:
    jdata = json.load(f)

if ('context' in jdata) and ('market_price_usd' in jdata['context']):
    usd_per_btc = int(jdata['context']['market_price_usd'])
    with open('coinprice.csv', 'a') as f:
        f.write("%d,%d\n" % (unixtime, usd_per_btc))

import numpy as np
import pandas as pd

# load from Catagolue:
df = pd.read_csv('newblocks.csv')

minblocks, maxblocks = min(df['id']), max(df['id'])
print('[%d, %d]' % (minblocks, maxblocks))

# append from Loyce if possible:
df2 = pd.read_csv('blockdata.csv')
if (len(df2) > 0) and (min(df2['id']) <= maxblocks) and (max(df2['id']) > maxblocks):
    df = pd.concat([df, df2[df.columns][df2['id'] > maxblocks]])

# prepend from Loyce if possible:
if (len(df2) > 0) and (max(df2['id']) >= minblocks) and (min(df2['id']) < minblocks):
    df = pd.concat([df2[df.columns][df2['id'] < minblocks], df])

minblocks, maxblocks = min(df['id']), max(df['id'])
print('[%d, %d]' % (minblocks, maxblocks))

# append from Blockchair if possible:
if ('data' in jdata) and jdata['data']:
    print('\033[32;1m%d blocks downloaded\033[0m' % len(jdata['data']))
    df2 = pd.DataFrame(jdata['data'])
    if (len(df2) > 0) and (min(df2['id']) <= maxblocks) and (max(df2['id']) > maxblocks):
        df = pd.concat([df, df2[df.columns][df2['id'] > maxblocks]])
else:
    print('\033[31;1mWarning: could not fetch JSON\033[0m')

minblocks, maxblocks = min(df['id']), max(df['id'])
print('[%d, %d]' % (minblocks, maxblocks))

df = df.sort_values('id')

last_df = []; dicts = []; dicts2 = []

for (k, sub_df) in df.set_index('id').groupby(lambda x : x // 48):
    unix_time = pd.to_datetime(sub_df['time'].iloc[0]).value // (10**9)

    if len(last_df) == 48:
        dicts.append( {'phase': last_k,
                       'start_time': last_time,
                       'end_time': unix_time,
                       'zero_bits': 32 + np.log2(last_df['difficulty'].iloc[0]),
                       'total_reward': int(last_df['reward'].sum()),
                       'total_reward_usd': int(last_df['reward_usd'].sum())})
        dicts2.append({'phase': last_k,
                       'transaction_count': int(last_df['transaction_count'].sum()),
                       'input_count': int(last_df['input_count'].sum()),
                       'output_count': int(last_df['output_count'].sum()),
                       'cdd_total': int(last_df['cdd_total'].sum()),
                       'output_total': int(1.0e-8 * last_df['output_total'].sum())})

    last_time, last_df, last_k = unix_time, sub_df, k

# truncate to last 2100 blocks:
if len(df) > 2100:
    df = df.iloc[-2100:]

df.to_csv('newblocks3.csv', index=False, float_format='%.6g')

for (name, d) in [("phases", dicts), ("onchain", dicts2)]:

    df = pd.DataFrame(d)
    df.to_csv(name + '2.csv', index=False, float_format='%.6g')

    phases = {}
    for fname in [name + '.csv', name + '2.csv']:
        with open(fname) as f:
            for l in f:
                if ',' in l:
                    phases[l.split(',')[0]] = l

    k = list(phases.keys())
    assert(k == ['phase'] + [str(n) for n in range(len(k)-1)])

    with open(name + '3.csv', 'w') as f:
        for v in phases.values():
            f.write(v)
